#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    std::vector<int> nums = {4,1,2,1,2};

    std::cout << "Input: ";
    for(auto& it: nums) {
        cout << it << ", ";
    } 
    std::cout << std::endl;

    Solution sol;
    int single_val = sol.singleNumber(nums);

    std::cout << "Result: " << single_val;
    std::cout << std::endl;
}