#include <algorithm>    // std::max
#include <vector>


class Solution {
public:
    void rotate(std::vector<int>& nums, int k);
    void print_vector(const std::vector<int>& nums) {
        for(auto& it: nums) {
            std::cout << it << ", ";
        } 
        std::cout << std::endl;
    }
};