#pragma once
#include <utility>
#include <unordered_map>
#include <vector>
#include <iostream>


/*
Source: https://www.geeksforgeeks.org/operations-sparse-matrices/
*/
class SparseMatrix 
{ 
    
    // Double-pointer initialized by  
    // the constructor to store  
    // the triple-represented form 
    //int **m_data; 
    std::vector<std::vector<int>> m_data;
  
    // dimensions of matrix 
    int m_row, m_col; 
  
    // total number of elements in matrix 
    int m_len; 
  
public: 
    SparseMatrix(int r, int c):
        m_data(0, std::vector<int>()),
        m_row(r),
        m_col(c),
        m_len(0) // initialize length to 0
    { 
  
        //Array of Pointer to make a matrix 
        //m_data = nullptr; 
  
        // Array representation 
        // of sparse matrix 
        //[,0] represents row 
        //[,1] represents col 
        //[,2] represents value 
        //or (int i = 0; i < 0; i++) 
        //    m_data[i] = new int[3]; 
    }

    SparseMatrix(int r, int c, int* data, int len):
        m_data(len, std::vector<int>(3, 0)),
        m_row(r),
        m_col(c),
        m_len(len) // initialize length
    {   
        //Array of Pointer to make a matrix 
        //m_data = new int *[m_len]; 
  
        // Array representation 
        // of sparse matrix 
        //[,0] represents row 
        //[,1] represents col 
        //[,2] represents value 
        //for (int i = 0; i < m_len; i++) 
        //    m_data[i] = new int[3];
        
        for (int i = 0; i < m_len; ++i) {
            for (int j = 0; j < 3; ++j) {
                m_data[i][j] = data[3 * i + j];
            }
        }
    }

    SparseMatrix(int r, int c, int len) :
        m_data(len, std::vector<int>(3, 0)),
        m_row(r),
        m_col(c),
        m_len(len) // initialize length to 0
    { 

    }

    ~SparseMatrix() {
        m_data.clear();
    }

    // Transpose
    SparseMatrix transpose() {
        // new matrix with inversed row X col 
        SparseMatrix result(m_col, m_row, m_len); 

        // same number of elements 
        result.m_len = m_len; 
  
        // to count number of elements in each column 
        int *count = new int[m_col + 1]; 
  
        // initialize all to 0 
        for (int i = 1; i <= m_col; i++) 
            count[i] = 0; 
  
        std::cout << "Fill histogram" << std::endl;

        for (int i = 0; i < m_len; i++) 
            count[m_data[i][1]]++; 
        
        std::cout << "Filled histogram" << std::endl;

        int *index = new int[m_col + 1]; 
  
        // initialize rest of the indices 
        for (int i = 1; i <= m_col; i++) 
              index[i] = index[i - 1] + count[i - 1]; 

        for (int i = 0; i < m_len; i++) 
        { 
  
            // insert a data at rpos and  
            // increment its value 
            int rpos = index[m_data[i][1]]++; 
  
            // transpose row=col 
            result.m_data[rpos][0] = m_data[i][1]; 
  
            // transpose col=row 
            result.m_data[rpos][1] = m_data[i][0]; 
  
            // same value 
            result.m_data[rpos][2] = m_data[i][2]; 
        }
        return result;
    }

    SparseMatrix multiply(SparseMatrix& b) {
        SparseMatrix b_t = b.transpose();

        int a_len = m_len;
        int b_len = b.m_len;

        int a_row = m_data[0][0];
        int b_col = b_t.m_data[0][0];

        SparseMatrix result(m_row, b.m_col);

        int apos, bpos; 
        for (apos = 0; apos < a_len;) 
        { 
            // current row of result matrix 
            int r = m_data[apos][0]; 

            std::cout << "row: " << r << std::endl;

            // iterate over all elements of B
            for (bpos = 0; bpos < b_len;) 
            { 
                // current column of result matrix 
                // data[,0] used as b is transposed 
                int c = b.m_data[bpos][0]; 
  
                std::cout << "col: " << r << std::endl;
                // temporary pointers created to add all 
                // multiplied values to obtain current 
                // element of result matrix 
                int tempa = apos; 
                int tempb = bpos; 
  
                int sum = 0;                 
                
                // iterate over all elements with 
                // same row and col value 
                // to calculate result[r] 
                while (tempa < m_len && m_data[tempa][0] == r && 
                       tempb < b.m_len && b_t.m_data[tempb][0] == c) 
                { 
                    if (m_data[tempa][1] < b_t.m_data[tempb][1]) 
  
                        // skip a 
                        tempa++; 
  
                    else if (m_data[tempa][1] > b_t.m_data[tempb][1]) 
  
                        // skip b 
                        tempb++; 
                    else
  
                        // same col, so multiply and increment 
                        sum += m_data[tempa++][2] *  
                               b_t.m_data[tempb++][2]; 
                } 

                std::cout << "sum: " << sum << std::endl;

                // insert sum obtained in result[r] 
                // if its not equal to 0 
                if (sum != 0) 
                    result.insert(r, c, sum); 
  
                while (bpos < b_t.m_len &&  
                       b_t.m_data[bpos][0] == c) 
  
                    // jump to next column 
                    bpos++; 
            }

            while (apos < m_len && m_data[apos][0] == r) 
                // jump to next row 
                apos++; 
        }

        return result;
    }

    void insert(int row, int col, int val) {
        std::vector<int> rcv = {row, col, val};
        m_data.push_back(rcv);
        m_len++;
    }

    // Debug
    void print_full() {
        int cur_row = 0;
        for (int i = 0; i < m_row; ++i) {
            int cur_col = 0;
            for (int j =0 ; j < m_col; ++j) {
                
            }
        }
    }

    void print() {
        for (int i = 0; i < m_len; ++i) {
            std::cout << "(" << m_data[i][0] << ", " << m_data[i][1] << ") -> " << m_data[i][2] << std::endl;
        }
    }

};
