#include <iostream>
#include <vector>
#include <string>

#include "solution.h"
#include "sparse_matrices.h"

using namespace std;


int main() {
    int A[] = {1, 2, 10,
               1, 4, 12,
               3, 3, 5,
               4, 1, 15,
               4, 2, 12};
    int B[] = {1, 3, 8,
               2, 4, 23,
               3, 3, 9,
               4, 1, 20,
               4, 2, 25};

    SparseMatrix As(4, 4, A, 5);
    SparseMatrix Bs(4, 4, B, 5);
    std::cout << "A before transpose: " << endl;
    As.print();
    SparseMatrix A_t = As.transpose();

    std::cout << "A after transpose: " << endl;
    A_t.print();

    SparseMatrix AB = As.multiply(Bs);
    
    std::cout << "A * B: " << endl;
    AB.print();
    
    std::cout << std::endl;
    return 0;
}