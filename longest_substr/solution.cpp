#include <iostream>
#include <algorithm>    // std::reverse
#include <unordered_set>
#include "solution.h"

using namespace std;

int Solution::lengthOfLongestSubstring(string s) {
    int max_len = 0;
    for(int i = 0; i < s.size(); ++i) {
        std::unordered_set<char> hash;
        int counter = 0;
        for (int j = i; j < s.size(); ++j, ++counter) {
            const bool is_in = hash.find(s[j]) != hash.end();
            if (is_in) {
                break;
            }
            else {
                hash.emplace(s[j]);
            }
        }
        cout << "i: " << i << ", counter: " << counter << endl;
        max_len = std::max(counter, max_len);
    }

    return max_len;
}
