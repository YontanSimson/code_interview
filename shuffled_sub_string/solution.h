#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <string>


class Solution {
public:

    int is_substring(std::string s, std::string a);    
};