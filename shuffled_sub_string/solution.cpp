#include <iostream>
#include <algorithm>    // std::reverse
#include <unordered_map>
#include "solution.h"

using namespace std;

void print_map(const std::unordered_map<char, int>& key_dict) {
    for(auto& it: key_dict) {
        cout << it.first <<": " <<it.second << endl;
    }
}

// Check is a is a shuffled substring of str
int Solution::is_substring(string str, string a) {
    bool condition = false;
    int len_str = str.size();
    int len_a = a.size();

    if (len_a > len_str) return false;

    std::unordered_map<char, int> key_dict;
    for (auto & it: a) {
        if (key_dict.count(it) == 0 ) {
            key_dict[it] = 1;
        }
        else {
            key_dict[it]++;
        }
    }
    cout << "key_dict: " << endl;
    print_map(key_dict);

    for(int i = 0; i < len_str - len_a; ++i) {
        std::unordered_map<char, int> a_dict;
        std::cout << "i: " << i << ", str[i] " << str[i] << endl;

        int j = 0; 
        for(; j < a.size(); ++j) {
            int c = str[i + j];
            if (a_dict.count(c) == 0 ) {
                a_dict[c] = 1;
            }
            else {
                a_dict[c]++;
            }
        }

        cout << "a_dict: " << endl;
        print_map(a_dict);

        if (a_dict == key_dict) {
            return true;
        }
    }

    return condition;
}
