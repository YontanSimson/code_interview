#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    vector<int> nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    // vector<int> a = {1,1,5};
    std::cout << "nums: " << endl;
    for (auto & it: nums) 
        cout << it << ", ";
    cout << endl;

    Solution* obj = new Solution(nums);
    vector<int> param_1 = obj->reset();
    vector<int> param_2 = obj->shuffle();
    obj->reset();
    vector<int> param_3 = obj->shuffle();
    
    std::cout << "Output1: " << std::endl;
    for (auto & it: param_2) 
        cout << it << ", ";
    cout << endl;

    std::cout << "Output2: " << std::endl;
    for (auto & it: param_3) 
        cout << it << ", ";
    cout << endl;

    delete obj;
    return 0;
}