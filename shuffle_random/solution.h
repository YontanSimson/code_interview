#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <string>
#include <ctime>
#include <random>
#include <iostream>

class Solution {

public:

    Solution(std::vector<int>& nums):
    m_nums(nums){
        
    }
    
    /** Resets the array to its original configuration and return it. */
    std::vector<int> reset() {
        return m_nums;
    }
    
    /** Returns a random shuffling of the array. */
    std::vector<int> shuffle() {
        std::cout << "Shuffle: " << std::endl;
        std::vector<int> temp_vec(m_nums.begin(), m_nums.end());
        std::cout << "nums.size: " << m_nums.size() << std::endl;
        std::cout << "temp_vec.size: " << temp_vec.size() << std::endl;
        // Randomly choose an index - Fisher-Yates algorithm: https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
        for (int i = temp_vec.size() - 1; i > 0; --i) {
            std::uniform_int_distribution<int> distribution(0, i);
            int j = distribution(m_generator);
            std::cout << "Swap - j:" << j << "" <<" i: " << i << std::endl;
            std::cout << temp_vec[j] << "<->" << temp_vec[j] << std::endl;
            // Swap i<->j
            int temp = temp_vec[i];
            temp_vec[i] = temp_vec[j];
            temp_vec[j] = temp;
        }
        return temp_vec;
    }
private:
    std::default_random_engine m_generator;
    std::vector<int>& m_nums;
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(nums);
 * vector<int> param_1 = obj->reset();
 * vector<int> param_2 = obj->shuffle();
 */
