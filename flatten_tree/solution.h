#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <stack>

#include "tree_node.h"


class Solution {
public:
    void flatten(TreeNode* root);
private:
    std::stack<TreeNode *> m_stack;
};