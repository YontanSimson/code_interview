#include <iostream>
#include <vector>
#include <string>
#include <stack>

#include "solution.h"
#include "tree_node.h"

using namespace std;


TreeNode* helper(TreeNode* root) {
    if (root == nullptr) {
        return nullptr;
    }

    //leaf node simply return as it is
    if (root->left == nullptr && root->right == nullptr) {
        return root;
    }

    // Recursively flatten the left subtree
    TreeNode* left = helper(root->left);

    // Recursively flatten the right subtree
    TreeNode* right = helper(root->right);

    // If there was a left subtree, we shuffle the connections
    // around so that there is nothing on the left side
    // anymore.
    if (left != nullptr) {
        left->right = root->right;
        root->right = root->left;
        root->left = nullptr;  
    }

    // We need to return the "rightmost" node after we are
    // done wiring the new connections.
    return right == nullptr ? left : right;

}


void Solution::flatten(TreeNode* root) {
    helper(root);
}


