#include <iostream>
#include <vector>
#include <string>

#include "solution.h"
#include "tree_node.h"

using namespace std;


static void inorderTrav(TreeNode* root) {
    if (root != NULL){
        inorderTrav(root->left);
        cout << root->val <<" ";
        inorderTrav(root->right);
    }
}


static void preorderTrav(TreeNode* root) {
   if (root != NULL){
        cout << root->val <<" ";
        preorderTrav(root->left);
        preorderTrav(root->right);
   }
}


int main()
{
    int vec1[] = {1, 2, 5, 3, 4, -1, 6};
    
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    root->right = new TreeNode(5);

    root->left->left = new TreeNode(3);
    root->left->right = new TreeNode(4);

    //root->right->left = new TreeNode(5);
    root->right->right = new TreeNode(6);

    std::cout << "Tree input: ";
    inorderTrav(root);
    std::cout << endl << "(preorder)Tree input: ";
    preorderTrav(root);

    Solution sol;
    sol.flatten(root);
    std::cout << endl << "Flattened list: ";
    inorderTrav(root);

    std::cout << std::endl;
    return 0;
}