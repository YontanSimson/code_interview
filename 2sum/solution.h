#pragma once

#include <algorithm>    // std::max
#include <vector>


class Solution {
public:
    std::vector<int> twoSum(std::vector<int>& nums, int target);
    
    void print_vector(const std::vector<int>& nums) {
        for(auto& it: nums) {
            std::cout << it << ", ";
        } 
        std::cout << std::endl;
    }
};