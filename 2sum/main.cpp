#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    std::vector<int> vec = {-10,-1,-18,-19};
    int target = -19;

    std::cout << "Input: ";
    for(auto& it: vec) {
        cout << it << ", ";
    } 
    std::cout << std::endl;

    Solution sol;
    std::vector<int> result = sol.twoSum(vec, target);

    std::cout << "Output: ";
    for(auto& it: result) {
        cout << it << ", ";
    } 
    std::cout << std::endl;
    return 0;
}