#include <iostream>
#include <algorithm>    // std::reverse
#include <cctype>
#include <cstdint>

#include "solution.h"

using namespace std;

// Find closest number with binary search - on a sorted array (descending)
int bsearch(const std::vector<int>& nums, int left, int right, int key) {
    int index = -1;
    std::cout << "left val: " << nums[left] << " ,  right val: " << nums[right] << endl;
    while (left <= right) {
        int mid = left + (right - left) / 2;
        if (key < nums[mid]) {
            left = mid + 1;
        }
        else
        {
            right = mid - 1;
        }
        if (index == -1 || nums[index] >= nums[mid])
            index = mid;
        std::cout << "left val: " << nums[left] << ", mid value: "<<  nums[mid] << " ,  right val: " << nums[right] << endl;
    }
    return index;
}

void reverse(std::vector<int>& nums, int left, int right) {
    while( left < right ) {
        std::swap(nums[++left], nums[--right]);
    }
}


void Solution::nextPermutation(std::vector<int>& nums) {
    int len = nums.size();
    int i = len - 2;
    // from back to front search for a numbers that are not descending
    for (; i >= 0; i--) {
        if (nums[i - 1] < nums[i]) {
            break;
        }
    }
    std::cout << "i: " << i << endl;
    if (i >= 0) {
        // swap with number on the right side closest to selected number (non descending)
        std::cout << "nums[i]: " << nums[i] << endl;
        //Fnding closest
        //int closest = bsearch(nums, i + 1, len - 1, nums[i]) ;

        int j = len - 1;
        while (j >= 0 && nums[j] <= nums[i]) {
            j--;
        }

        cout << "Closest number: " <<  nums[j]  << endl;

        // Now swap key with closest number
        std::cout << "Swap: " << nums[i] << ", " << nums[j] << std::endl;
        
        std::swap(nums[i], nums[j]);
    }
    // Reverse the rest
    reverse(nums, i + 1, len - 1);

}
