#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    vector<int> a = {1, 2, 3};
    // vector<int> a = {1,1,5};
    std::cout << "a: " << endl;
    for (auto & it: a) 
        cout << it << ", ";
    cout << endl;
    
    Solution sol;
    sol.nextPermutation(a);

    std::cout << "Output: " << std::endl;
    for (auto & it: a) 
        cout << it << ", ";
    cout << endl;

   return 0;
}