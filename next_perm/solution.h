#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <string>


class Solution {
public:
    void nextPermutation(std::vector<int>& nums);
};