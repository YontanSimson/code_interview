#include <iostream>
#include <algorithm>    // std::reverse
#include "solution.h"

using namespace std;

int Solution::singleNumber(std::vector<int>& nums) {
    if (nums.size() == 0) return -1;
    if (nums.size() == 1) return nums[0];
    
    for(auto& it: nums) {
        std::cout << it << ", ";
    } 
    std::cout << std::endl;

    std::sort (nums.begin(), nums.end());

    if (nums[0] != nums[1]) {
        return nums[0];
    }

    if (nums[nums.size() - 2] != nums[nums.size() - 1]) {
        return nums[nums.size() - 1];
    }

    for (int i = 1; i < nums.size() - 1; ++i) {
        if (nums[i] != nums[i + 1]  && nums[i] != nums[i - 1])
            return nums[i];
    }

    return -1;
}
