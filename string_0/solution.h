#include <algorithm>    // std::max
#include <vector>


class Solution {
public:
    void reverseString(std::vector<char>& s);
    
    void print_vector(const std::vector<int>& nums) {
        for(auto& it: nums) {
            std::cout << it << ", ";
        } 
        std::cout << std::endl;
    }
};