#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    std::vector<char> s_array = {'h','e','l','l','o'};

    std::cout << "Input: ";
    for(auto& it: s_array) {
        cout << it << ", ";
    } 
    std::cout << std::endl;

    Solution sol;
    sol.reverseString(s_array);

    std::cout << "Output: ";
    for(auto& it: s_array) {
        cout << it << ", ";
    } 
    std::cout << std::endl;
    return 0;
}