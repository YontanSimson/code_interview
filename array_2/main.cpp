#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    std::vector<int> nums = {1,2,3,4,5,6,7};
    int k = 3;

    std::cout << "Before: ";
    for(auto& it: nums) {
        cout << it << ", ";
    } 
    std::cout << std::endl;

    Solution sol;
    sol.rotate(nums, k);

    std::cout << "After: ";
    for(auto& it: nums) {
        cout << it << ", ";
    }
    std::cout << std::endl;
}