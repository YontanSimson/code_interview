#include <iostream>
#include <algorithm>    // std::reverse
#include "solution.h"

using namespace std;

void Solution::rotate(std::vector<int>& nums, int k) {
    if (k == 0 || nums.size() == 0) return;
    k = k % nums.size();
    cout << "Start " << endl;
    
    std::reverse(nums.begin(), nums.end()); 
    print_vector(nums);
    std::reverse(nums.begin(), nums.begin() + k);
    print_vector(nums);
    std::reverse(nums.begin() + k, nums.end()); 
    print_vector(nums);
    // for(int idx = 0; idx < k; ++idx) {
    //     int temp = nums.back();
    //     std::cout << "idx: " << idx << endl;
    //     for (int i = nums.size() - 2; i >= 0; --i) {
    //         std::cout << "i: " << i << endl;
    //         nums[i + 1] = nums[i];

    //         print_vector(nums);
    //     }
    //     cout << "temp: " << temp << std::endl;
    //     nums[0] = temp;
    // }
}
