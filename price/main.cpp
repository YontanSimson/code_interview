#include <iostream>
#include <vector>
#include <string>
#include "price.h"

using namespace std;

int main()
{
    vector<int> prices{1, 2, 3, 4, 5};

    // print input
    for(auto& it: prices) {
        std::cout << it << ", ";
    }
    std::cout << endl;

    Solution solver;
    int profit = solver.maxProfit(prices);
    cout << "Profit: "<< profit << endl;
}