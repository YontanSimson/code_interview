#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <iomanip>

#include "price.h"

using namespace std;


int Solution::maxProfit(vector<int>& prices) {
    if (prices.size() == 0) return 0;
    
    int num = prices.size();
    // vector<vector<int>> price_diff(num, vector<int>(num));

    // for (int i = 0; i < num; ++i) {
    //     for (int j = 0; j < num; ++j) {
    //         cout << std::right << std::setw(2) << prices[j] - prices[i] << ", ";
    //         if (i > j) continue;
    //         price_diff[i][j] = prices[j] - prices[i];
    //     }
    //     cout << endl;
    // }
    
    // Summarize positve diagonals in upper right triangle
    int price_sum = 0;
    int diag_idx = 1; 
    std::cout << "diag_idx: " << diag_idx << endl;
    for (int i = 0; i < num - diag_idx; ++i) {
        int gain = prices[i + diag_idx] - prices[i];
        if ( gain > 0){
            std::cout << "+ " << gain;
            price_sum += gain;
        }
    }
    std::cout << endl;
    
    return price_sum;
}