#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;

int main()
{
    std::vector<int> nums1 = {1,2,3,0,0,0};
    std::vector<int> nums2 = {2,5,6};

    std::cout << "Input 1: ";
    for (auto & it: nums1) cout << it << ", ";
    std::cout << std::endl;

    std::cout << "Input 2: ";
    for (auto & it: nums2) cout << it << ", ";
    std::cout << std::endl;

    Solution sol;
    sol.merge(nums1, 3, nums2, nums2.size());

    std::cout << std::endl << "Output: ";
    for (auto & it: nums1) cout << it << ", ";

}