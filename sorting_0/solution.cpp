#include <iostream>
#include "solution.h"    // std::max

using namespace std;

void Solution::merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
    // Put nums2 into nums1
    // Both are sorted. Merge them into a single sorted array
    int j = 0;
    int i = 0;
    while (i < m || j < n) {
        if (nums1[i] >= nums2[j]){
            for (int l = i; l < m; ++l){
                nums1[i + 1] = nums1[i];
            }
            nums1[i] = nums2[j];
            j++;
            m++;
        }
        else {
            i++;
        }
        std::cout << "i: " << i << " j: " << j << std::endl;
        for (auto & it: nums1) cout << it << ", ";
        std::cout << std::endl;

    }
}
