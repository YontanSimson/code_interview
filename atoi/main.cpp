#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    string a = "   -42";
    cout << "a: " << a << endl;
    
    Solution sol;
    int result = sol.myAtoi(a);


    std::cout << "Output: " << result << std::endl;

    return 0;
}