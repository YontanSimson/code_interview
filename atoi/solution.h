#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <string>


class Solution {
public:
    int myAtoi(std::string str);
};