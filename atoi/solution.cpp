#include <iostream>
#include <algorithm>    // std::reverse
#include <cctype>
#include <cstdint>

#include "solution.h"

using namespace std;

int Solution::myAtoi(string str) {
        int sign = 1, base = 0, i = 0; 
        // if whitespaces then ignore. 
        while (str[i] == ' ') { 
            i++; 
        } 
        // sign of number 
        if (str[i] == '-' || str[i] == '+') { 
            sign = 1 - 2 * (str[i++] == '-'); 
        } 
        // checking for valid input 
        while (str[i] >= '0' && str[i] <= '9') { 
            // handling overflow test case 
            if (base > INT32_MAX / 10 || (base == INT32_MAX / 10 && str[i] - '0' > 7)) { 
                if (sign == 1) 
                    return INT32_MAX; 
                else
                    return INT32_MIN; 
            } 
            base = 10 * base + (str[i++] - '0'); 
        } 
        
        return int(base * sign);         
}
