#pragma once
#include <algorithm>    // std::max
#include <vector>
#include 

#include "tree_node.h"


class Solution {
public:
    bool isValidBST(TreeNode* root);
    
};