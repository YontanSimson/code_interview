#include <iostream>
#include <vector>
#include <string>
#include <stack>
#include <climits>

#include "solution.h"
#include "tree_node.h"

using namespace std;



bool helper(TreeNode* root, int64_t low, int64_t high) {
    if (root == nullptr) return true;

    int val = root->val;
    cout << "val: " << val <<endl;
    cout << "high: " << high <<endl;
    cout << "low: " << low <<endl;

    if (val <= low) return false;
    if (val >= high) return false;

    std::cout << "Check right: " << endl;
    if ( !helper(root->right, val, high) ) return false;

    std::cout << "Check left: " << endl;
    if ( !helper(root->left, low, val)) return false;

    return true;
 
}

bool Solution::isValidBST(TreeNode* root) {
    return helper(root, INT64_MIN, INT64_MAX); 
}

