#include <iostream>
#include <algorithm>    // std::reverse
#include <unordered_set>
#include "solution.h"

using namespace std;

// Check is a is a substring of str
int Solution::is_substring(string str, string a) {
    bool condition = false;
    int len_str = str.size();
    int len_a = a.size();

    if (len_a > len_str) return false;

    for(int i = 0; i < len_str - len_a; ++i) {
        std::cout << "i: " << i << ", str[i]" << str[i] << endl;

        int j = 0; 
        for(; j < a.size(); ++j) {
            if (a[j] != str[i + j]) break;
            cout << "a[i]: " << a[i] << " , str[i + j]: " << str[i + j] << endl;
        }

        std::cout << "j:" << j << endl;
        if (j == a.size()) return true;
    }

    return condition;
}
