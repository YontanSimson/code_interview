#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    string str = "abcabcbb";
    string a = "cab";
    cout << "str: " << str << endl;
    cout << "a: " << a << endl;
    
    Solution sol;
    bool sub = sol.is_substring(str, a);

    std::cout << "sub: " << sub << std::endl;

    return 0;
}