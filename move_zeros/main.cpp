#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    std::vector<int> vec1 = {0,1,0,3,12};

    std::cout << "Input: " << endl;
    Solution::print_vector(vec1);

    Solution sol;
    sol.moveZeroes(vec1);

    std::cout << "Output: ";
    Solution::print_vector(vec1);
    std::cout << std::endl;
    return 0;
}