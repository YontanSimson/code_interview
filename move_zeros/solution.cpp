#include <iostream>
#include <algorithm>    // std::reverse
#include "solution.h"

using namespace std;

void Solution::moveZeroes(vector<int>& nums){
    int zero_idx = 0;
    for (int i = 0; i < nums.size(); ++i) {
        if (nums[i] != 0 ) {
            nums[i - zero_idx] = nums[i];
        }
        else {
            zero_idx++;
        }
    }

    for (int i = nums.size() - zero_idx; i < nums.size(); ++i) {
        nums[i] = 0;
    }

}

