#include <iostream>
#include <algorithm>    // std::reverse
#include <unordered_set>
#include "solution.h"

using namespace std;

int Solution::lengthOfLongestSubstring(string s) {

    int n = s.size();
    std::unordered_set<char> set;
    int ans = 0, i = 0, j = 0;
    while (i < n && j < n) {
        // try to extend the range [i, j]
        if (set.count(s[j]) == 0 ) {
            set.emplace(s[j++]);
            ans = std::max(ans, j - i);
        }
        else {
            set.erase(s[i++]);
        }
    }

    return ans;
}
