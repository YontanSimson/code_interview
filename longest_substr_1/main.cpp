#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    string a = "abcabcbb";
    cout << "a: " << a << endl;
    
    Solution sol;
    int length = sol.lengthOfLongestSubstring(a);

    std::cout << "length: " << length << std::endl;

    return 0;
}