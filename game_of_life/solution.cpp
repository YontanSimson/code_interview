#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


void printMatrix(const std::vector<std::vector<int>>& matrix) {
    for(auto& row: matrix) {
        for (auto& col: row) {
            cout << col << ", ";
        }
        cout << endl;
    }
}

int main()
{   
    std::vector<std::vector<int>> matrix {
				{ 0, 1, 0 },
				{ 0, 0, 1 },
				{ 1, 1, 1 },
                { 0, 0, 0 }
			};

    cout << "board: " << endl;
    printMatrix(matrix);
    
    Solution sol;
    sol.gameOfLife(matrix);

    cout << "Next board: " << endl;
    printMatrix(matrix);

    return 0;
}