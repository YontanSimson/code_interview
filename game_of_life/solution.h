#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <iostream>



class Solution {
public:
    void gameOfLife(std::vector<std::vector<int>>& board) {
        /*
        Any live cell with fewer than two live neighbors dies, as if caused by under-population.
        Any live cell with two or three live neighbors lives on to the next generation.
        Any live cell with more than three live neighbors dies, as if by over-population..
        Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
        */

        int rows = board.size();
        for (int i = 0; i < rows; ++i) {
            int cols = board[i].size();
            for (int j = 0; j < cols; j++) {
                int live = countNeighbors(board, i, j);
                if ( board[i][j] == 1 ) {
                    if (live < 2 || live > 3 ) {
                        board[i][j] = -1;
                    }
                }
                if (live == 3 && board[i][j] == 0) {
                    board[i][j] = 2;
                }
            }
        }

        for(auto& row: board) {
            for (auto& col: row) {
                col = (col > 0) ? 1 : 0;
            }
        }
    }
private:
    int countNeighbors(std::vector<std::vector<int>>& board, int i, int j) {
        int cols = board[i].size();
        int rows = board.size();
        m_live = 0;

        int start_x = -1, end_x = 2, start_y = -1, end_y = 2;
        if (i == 0) start_y = 0;
        if (j == 0) start_x = 0;
        if (i == rows - 1) end_y = 1;
        if (j == cols - 1) end_x = 1;

        for (int y = start_y; y < end_y; ++y) {
            for (int x = start_x; x < end_x; ++x) {
                if (x == 0 && y == 0) continue;
                if (std::abs(board[i + y][j + x]) == 1) m_live++;
            }
        }

        return m_live;
    }
    int m_dead;
    int m_live;

};
