#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    string a = "1";
    string b = "11";
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    
    Solution sol;
    string c = sol.addBinary(a, b);


    std::cout << "Output: " << c << std::endl;

    return 0;
}