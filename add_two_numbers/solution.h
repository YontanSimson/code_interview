#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <string>


class Solution {
public:
    int convert_bit(char& a) {
        return a - '0';
    }

    int add_bits(int a, int b){
        int sum = a + b + m_carry;
        m_carry = sum / BASE;
        return sum % BASE;
    }

    int m_carry = 0;
    static const int BASE = 2;

    std::string addBinary(std::string a, std::string b) {
        m_carry = 0;
        std::vector<int> a_vec;
        std::vector<int> b_vec;

        std::vector<char> output_reversed;

        for (auto it = a.rbegin(); it != a.rend(); ++it) {
            a_vec.push_back(convert_bit(*it));
        }

        for (auto it = b.rbegin(); it != b.rend(); ++it) {
            b_vec.push_back(convert_bit(*it));
        }
        
        auto a_it = a_vec.begin();
        auto b_it = b_vec.begin();
        while (a_it != a_vec.end() || b_it != b_vec.end()) {

            int add_result;
            if ( a_it != a_vec.end() && b_it != b_vec.end() ) {
                add_result = add_bits(*a_it, *b_it);
            }
            else if (a_it != a_vec.end()) {
                add_result = add_bits(*a_it, 0);
            }
            else {
                add_result = add_bits(0, *b_it);
            }

            std::cout << "sum: " << add_result << std::endl;
            std::cout << "carry: " << m_carry << std::endl;
            
            output_reversed.push_back(add_result + '0');


            if ( a_it != a_vec.end() ) ++a_it;
            if ( b_it != b_vec.end() ) ++b_it;
        }
        
        if ( m_carry > 0 ) output_reversed.push_back(m_carry + '0');

        const int out_length = output_reversed.size();
        char* out = new char[out_length + 1];

        for (int i = 0; i < out_length; ++i) {
            out[i] = output_reversed[out_length - 1 - i];
        }
        out[out_length] = '\0';

        return std::string(out);
    }

    
    void print_vector(const std::vector<int>& nums) {
        for(auto& it: nums) {
            std::cout << it << ", ";
        } 
        std::cout << std::endl;
    }
};