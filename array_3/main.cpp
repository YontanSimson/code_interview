#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    std::vector<int> nums = {1,2,3,4};

    std::cout << "Input: ";
    for(auto& it: nums) {
        cout << it << ", ";
    } 
    std::cout << std::endl;

    Solution sol;
    bool repeats = sol.containsDuplicate(nums);

    std::cout << "Result: " << repeats;
    std::cout << std::endl;
}