#include <iostream>
#include <algorithm>    // std::reverse
#include "solution.h"

using namespace std;

bool Solution::containsDuplicate(std::vector<int>& nums) {
    if (nums.size() == 0) return false;

    // make a histogram - hash table would be the best structure. Using simple array
    int max_val = *std::max_element(nums.begin(), nums.end());
    int min_val = *std::min_element(nums.begin(), nums.end());
    std::vector<int> hist(max_val - min_val + 1, 0);
    for (int i = 0; i < nums.size(); ++i) {
        int val = nums[i] - min_val;
        hist[val]++;
        if ( hist[val] > 1 ) return true;
    }
    return false;
}
