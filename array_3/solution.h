#include <algorithm>    // std::max
#include <vector>


class Solution {
public:
    bool containsDuplicate(std::vector<int>& nums);
    
    void print_vector(const std::vector<int>& nums) {
        for(auto& it: nums) {
            std::cout << it << ", ";
        } 
        std::cout << std::endl;
    }
};