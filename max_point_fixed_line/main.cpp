#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;
/*
Given a set of N points (x1,x2,x3,...,xn) and L the fixed length of a segment.

Find the number of maximum points which you can cover with a segment line of length L.

*/


int main()
{   

    std::vector<int> vec {0, 1, 3, 4, 5, 6, 8, 11, 13};

    for(auto& it: vec) {
        cout << it << ", ";
    }
    cout << endl;

    int L = 4;
    
    Solution sol;
    std::vector<int> sub_vec = sol.getMaxPoints(vec, L);

    cout << "Results: " << endl;
    for(auto& it: sub_vec) {
        cout << it << ", ";
    }
    cout << endl;

    return 0;
}