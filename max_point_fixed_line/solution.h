#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <iostream>



class Solution {
public:
    std::vector<int> getMaxPoints(std::vector<int>& vec, int L);
};
