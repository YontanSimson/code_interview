#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

#include "solution.h"

using namespace std;

std::vector<int> Solution::getMaxPoints(std::vector<int>& vec, int L) {

    std::map<int, int> pointsInL;
    
    int max_count = 0;
    int start_x = 0;
    int end_x = L - 1;
    for (int i = 0; i < vec.size() - L; ++i) {
        int start_val = vec[i];
        // Look ahead
        //int count = 0;
        int j = 0;
        for (; j < L; ++j) {
            int val = vec[i + j];

            auto it = pointsInL.find(val);
            if (it == pointsInL.end())
                pointsInL[val] = j + i;

            if ( val - start_val + 1 > L) {
                break;
            }
            //count++;
        }
        std::cout << "pointsInL.size: " << pointsInL.size() << endl;

        pointsInL.erase(start_val);  // erasing by key

        std::cout << "i: " << i << ", j: " << j << ", count: " << j << endl;

        if (j >= max_count) {
            max_count = j;
            start_x = i;
            end_x = i + j;
        }
    }
    std::cout << "start_x: " << start_x << ", end_x: " << end_x << ", max_count: " << max_count << endl;
    return vector<int>(vec.begin() + start_x, vec.begin() + end_x);
}
