#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include "solution.h"

using namespace std;


int main()
{
    std::vector<int> vec = {-1, 0, 1, 2, -1, -4};
    int target = 1;

    std::sort(vec.begin(), vec.end());

    std::cout << "Input: ";
    for(auto& it: vec) {
        cout << it << ", ";
    } 
    std::cout << std::endl;

    Solution sol;
    std::vector<int> result = sol.twoSum(vec, target);

    std::cout << "Output: ";
    for(auto& it: result) {
        cout << it << ", ";
    } 
    std::cout << std::endl;
    return 0;
}