#include <iostream>
#include <algorithm>    // std::reverse
#include <unordered_map>
#include "solution.h"

using namespace std;

vector<int> Solution::twoSum(vector<int>& nums, int target) {
    
    int low = 0;
    int high = nums.size() - 1;


    while(low < high) {
        if (nums[low] + nums[high] < target) {
            low++;
        }
        if (nums[low] + nums[high] > target) {
            high--;
        }

        if (nums[low] + nums[high] == target) {
            std::vector<int> indices = {low, high};
            return indices;
        }
    }

    // vector<int> indexes;

    // for (int x = 0; x < nums.size(); x++) {
    //     indexes.push_back(x);
    // }

    // sort( indexes.begin(), indexes.end(), [&](int i,int j){return nums[i] < nums[j];} );
    
    // std::cout << "Sorted indexes: " << std::endl;
    // for(auto& it: indexes) {
    //     std::cout << it << ", ";
    // }
    // std::cout << std::endl;

    // std::sort(nums.begin(), nums.end());
    
    // for (int i = 0; i < nums.size() - 1; ++i) {
    //     int x = nums[i];
    //     int y = target - x;
    //     std::cout << "x: " << x << ", Looking for " << y << std::endl;
    //     //search for y in nums - bin search
    //     auto low = std::lower_bound (nums.begin() + i + 1, nums.end(), y); // bin search
    //     std::cout << "low: " << *low << endl;
    //     if (*low == y) {
    //         std::cout << "Found: " << low - nums.begin() << std::endl;
    //         std::vector<int> indices;
    //         indices.push_back(indexes[i]);
    //         indices.push_back(indexes[int(low - nums.begin())]);
    //         return indices;
    //     }
        
    // }
    
    return vector<int>();
}
