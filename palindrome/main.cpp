#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    string a = "A man, a plan, a canal: Panama";
    //string a = "race a car";
    cout << "a: " << a << endl;
    
    Solution sol;
    bool is_palindrome = sol.isPalindrome(a);
    std::cout << "Output: " << is_palindrome << std::endl;

    return 0;
}