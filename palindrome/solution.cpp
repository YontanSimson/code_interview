#include <iostream>
#include <algorithm>    // std::reverse
#include <cctype>
#include "solution.h"

using namespace std;

bool Solution::isPalindrome(string s) {
    int rit = s.size() - 1;
    int it = 0;

    //cout << "checking" <<endl;
    while (rit >= it)  {
        //cout << "it " << it << endl;
        //cout << "rit " << rit <<endl;

        //Skip non ABC letters
        if ( !isalnum(s[rit]) ) {
            rit--;
            continue;
        }

        if ( !isalnum(s[it]) ) {
            it++;
            continue;
        }

        //cout << "s[it] " << s[it] << endl;
        //cout << "s[rit] " << s[rit] <<endl;

        // check if palindrome
        if (tolower(s[it]) != tolower(s[rit])) {
            return false;
        }
        it++;
        rit--;
     }
    return true;
}