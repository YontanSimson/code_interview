#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <string>


class Solution {
public:
    bool isPalindrome(std::string s);
    
 };