#pragma once
#include <algorithm>    // std::max
#include <vector>

#include "linked_node.h"


class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2);

    
    void print_vector(const std::vector<int>& nums) {
        for(auto& it: nums) {
            std::cout << it << ", ";
        } 
        std::cout << std::endl;
    }
};