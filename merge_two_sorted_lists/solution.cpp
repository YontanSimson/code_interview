#include <iostream>
#include <vector>
#include <string>

#include "solution.h"
#include "linked_node.h"

using namespace std;


static void print_linked_list(ListNode* v1) {
    ListNode* ptr = v1;
    while ( ptr != nullptr ) {
        cout << ptr->val << "->";
        ptr = ptr->next;
    }
    std::cout << std::endl;
}

int main()
{
    std::vector<int> vec1 = {1, 2, 4};
    std::vector<int> vec2 = {1, 3, 4};

    ListNode* v1 = createListFromArray(vec1);
    ListNode* v2 = createListFromArray(vec2);

    std::cout << "Input1: ";
    print_linked_list(v1);
    std::cout << "Input2: ";
    print_linked_list(v2);

    Solution sol;
    ListNode* v3 = sol.mergeTwoLists(v1, v2);

    std::cout << "Output: ";
    print_linked_list(v3);

    // ListNode* ptr = v1;
    // while ( ptr != nullptr ) {
    //     cout << ptr->val << "->";
    //     ptr = ptr->next;
    // } 

    std::cout << std::endl;
    return 0;
}