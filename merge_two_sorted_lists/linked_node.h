#pragma once

#include <vector>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

// Help function
static ListNode* createListFromArray(std::vector<int> vec);

ListNode* createListFromArray(std::vector<int> vec)
{
    ListNode* nodePtr = NULL;
    ListNode* rootNodePtr = NULL;
    ListNode* lastNodePtr = NULL;
    for(int i = 0 ; i < vec.size(); i++)
    {
        if(!nodePtr)
        {
            nodePtr = new ListNode(vec[i]);
            if(!rootNodePtr)
                rootNodePtr = nodePtr;
            if(lastNodePtr)
                lastNodePtr->next = nodePtr;
        }
        lastNodePtr = nodePtr;
        nodePtr = nodePtr->next;
    }
    return rootNodePtr;
}
 