#include <iostream>
#include <vector>
#include <string>

#include "solution.h"

using namespace std;


int main()
{
    //string a = "123";
    //string b = "456";
    string a = "9133";
    string b = "0";
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    
    Solution sol;
    string c = sol.multiply(a, b);


    std::cout << "Output: " << c << std::endl;

    return 0;
}