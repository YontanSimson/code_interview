#pragma once
#include <algorithm>    // std::max
#include <vector>
#include <string>


class Solution {
public:
    int convert_digit(char& a) {
        return a - '0';
    }

    int add_digits(int a, int b){
        int sum = a + b + m_carry;
        m_carry = sum / BASE;
        return sum % BASE;
    }

    int multiply_digits(int a, int b){
        int prod = a * b + m_carry;
        m_carry = prod / BASE;
        return prod % BASE;
    }

    int m_carry = 0;
    static const int BASE = 10;

    std::string multiply(std::string a, std::string b) {
        m_carry = 0;
        std::vector<int> a_vec; // digits reversed
        std::vector<int> b_vec; // digits reversed

        std::vector<char> output_reversed;

        for (auto it = a.rbegin(); it != a.rend(); ++it) {
            a_vec.push_back(convert_digit(*it));
        }

        for (auto it = b.rbegin(); it != b.rend(); ++it) {
            b_vec.push_back(convert_digit(*it));
        }
        
        std::vector<int> results;
        int level = 0;
        for(int i = 0; i < a_vec.size(); ++i) {
            std::vector<int> temp_results(level, 0);
            for(int j = 0; j < b_vec.size(); ++j) {
                int prod = multiply_digits(a_vec[i], b_vec[j]);
                temp_results.push_back(prod);
            }
            if ( m_carry > 0 ) temp_results.push_back(m_carry);
            m_carry = 0;

            // Add results and temp_results together
            if (results.size() == 0) {
                results = temp_results;
            } else {
                auto temp_it = temp_results.cbegin();
                auto results_it = results.cbegin();
                
                std::vector<int> add_results;
                while (temp_it != temp_results.end() || results_it != results.end()) {
                    if (temp_it != temp_results.end() && results_it != results.end()) {
                        add_results.push_back(add_digits(*temp_it, *results_it));
                        ++temp_it;
                        ++results_it;
                    }
                    else if (temp_it == temp_results.end() && results_it != results.end()) {
                        add_results.push_back(add_digits(0, *results_it));
                        ++results_it;
                    } else if (temp_it != temp_results.end() && results_it == results.end()) {
                        add_results.push_back(add_digits(*temp_it, 0));
                        ++temp_it;
                    }
                    else {
                        break;
                    }
                }

                if (m_carry > 0) add_results.push_back(m_carry);
                m_carry = 0;

                results = add_results;
            }

            level++;
        }
        
        // Filter leading zeros
        int out_length = results.size();
        for (int i = out_length - 1; i > 0; --i) {
            if (results[i] == 0) 
                results.pop_back();
            else 
                break; 
        }
        out_length = results.size();

        char* out = new char[out_length + 1];
        for (int i = 0; i < out_length; ++i) {
            out[i] = results[out_length - 1 - i] + '0';
        }
        out[out_length] = '\0';

        return std::string(out);
    }

    
    void print_vector(const std::vector<int>& nums) {
        for(auto& it: nums) {
            std::cout << it << ", ";
        } 
        std::cout << std::endl;
    }
};